<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
<?php
$DB_DSN = "mysql:host=localhost; dbname=kennsyu_tesut; charset=utf8";
$DB_USER = "itagaki";
$DB_PW =  "pGVRdU7mQWnFxeGo";
$pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);

include("./include/statics.php");

$query_str = "SELECT member_ID, name, pref, seibetu, age, section_ID, grade_ID
              FROM member
              LEFT JOIN section1_master ON section_ID = section_ID
              LEFT JOIN grade_master ON grade_ID = grade_ID
              WHERE member_ID ='". $_GET['member_ID']."'";

  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();
 ?>
 <?php
 $search_member_id = "";
 $search_name = "";
 $search_seibetu = "";
 $search_pref = "";
 $search_age = "";
 $search_section_ID = "";
 $search_grade_ID = "";


 if(isset($_GET['member_ID']) && $_GET['member_ID'] != ""){
   $search_member_id = $_GET['member_ID'];
 }
 if(isset($_GET['name']) && $_GET['name'] != ""){
   $search_name = $_GET['name'];
 }
 if(isset($_GET['seibetu']) && $_GET['seibetu'] != ""){
   $search_seibetu = $_GET['seibetu'];
 }
 if(isset($_GET['section_ID']) && $_GET['section_ID'] != ""){
   $search_section_ID = $_GET['section_ID'];
 }
 if(isset($_GET['pref']) && $_GET['pref'] != ""){
   $search_pref = $_GET['pref'];
 }
 if(isset($_GET['age']) && $_GET['age'] != ""){
   $search_age = $_GET['age'];
 }
 if(isset($_GET['grade_ID']) && $_GET['grade_ID'] != ""){
   $search_grade_ID = $_GET['grade_ID'];
 }
 ?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>新規社員情報登録（#04）</title>
  </head>
  <script type="text/javascript">
  <!--
  function conf(){
    //名前からスペースの除去、それから入力チェック
    var temp_name = document.update.name.value;
    temp_name =  temp_name.replace(/\s+/g,"");
    document.update.name.value = temp_name;
    if( temp_name == ""){
      alert("名前は必須です");
      return false;
    }
    //年齢上限下限チェック
    var temp_age = document.update.age.value;
    // alert(temp_age);
    if( temp_age == "" || Number.isInteger(temp_age)
        || temp_age < 1 || temp_age > 100){
      alert("年齢は必須です/数値を入力してください/1-99の範囲で入力してください");
      return false;
    }
    if(document.update.pref.value == ""){
      alert("都道府県は必須です");
      return false;
    }
    if(window.confirm('更新を行います。よろしいですか？')){
      document.update.submit();
      // alert("true");
    } else {
      // alert("false");
    }
  }
  -->
  </script>
  <body>
    <?php include("./include/header.php"); ?>
    <form method="GET" action="update02.php" name="update">
      <div class="result_wrap detail_result" id="tbl-bdr">
        <table border="1" align="center" style="border-collapse:collapse;">
          <tr>
            <th>社員ID</th>
            <td>
              <?php echo $result[0]['member_ID'];?>
            </td>
          </tr>
          <tr>
            <th>名前</th>
            <td><input type="text" name="name" value="<?php echo $result[0]['name'];?>" required></td>
          </tr>
          <tr>
            <th>出身地</th>
            <td>
              <select name="pref" required>
                <?php
                 foreach($pref_array as $key => $value){
                   echo "<option value='" . $key . "'";
                   if($result[0]['pref'] == $key){ echo " selected ";}
                   echo ">" . $value . "</option>";
                 }
                 ?>
              </select>
            </td>
          </tr>
          <tr>
            <th>性別</th>
            <td>
              <input type='radio' name='seibetu' <?php if ($result[0]['seibetu'] == "1") { echo "checked"; } ?> value='1' id="sex1"><label for="sex1">男</label>
              <input type='radio' name='seibetu' <?php if ($result[0]['seibetu'] == "2") { echo "checked"; } ?> value='2' id="sex2"><label for="sex2">女</label>
              <input type='radio' name='seibetu' <?php if ($result[0]['seibetu'] == "3") { echo "checked"; } ?> value='3' id="sex3"><label for="sex3">その他</label>
            </td>
          </tr>
          <tr>
            <th>年齢</th>
            <td><input type="number" name="age" value="<?php echo $result[0]['age'];?>" max="99" min="1" required></td>
          </tr>
          <tr>
            <th>所属部署</th>
            <td>
            <input type='radio' name='section_ID' <?php if ($result[0]['section_ID'] == "1") { echo "checked"; } ?> value='1' id="sec1"><label for='sec1'>第一事業部</label>
            <input type='radio' name='section_ID' <?php if ($result[0]['section_ID'] == "2") { echo "checked"; } ?> value='2' id="sec2"><label for='sec2'>第二事業部</label>
            <input type='radio' name='section_ID' <?php if ($result[0]['section_ID'] == "3") { echo "checked"; } ?> value='3' id="sec3"><label for='sec3'>営業</label>
            <input type='radio' name='section_ID' <?php if ($result[0]['section_ID'] == "4") { echo "checked"; } ?> value='4' id="sec4"><label for='sec4'>総務</label>
            <input type='radio' name='section_ID' <?php if ($result[0]['section_ID'] == "5") { echo "checked"; } ?> value='5' id="sec5"><label for='sec5'>人事</label>
            </td>
          </tr>
          <tr>
            <th>役職</th>
            <td>
              <input type='radio' name='grade_ID' <?php if ($result[0]['grade_ID'] == "1") { echo "checked"; } ?> value='1' id="gra1"><label for='gra1'>事業部長</label>
              <input type='radio' name='grade_ID' <?php if ($result[0]['grade_ID'] == "2") { echo "checked"; } ?> value='2' id="gra2"><label for='gra2'>部長</label>
              <input type='radio' name='grade_ID' <?php if ($result[0]['grade_ID'] == "3") { echo "checked"; } ?> value='3' id="gra3"><label for='gra3'>チームリーダー</label>
              <input type='radio' name='grade_ID' <?php if ($result[0]['grade_ID'] == "4") { echo "checked"; } ?> value='4' id="gra4"><label for='gra4'>リーダー</label>
              <input type='radio' name='grade_ID' <?php if ($result[0]['grade_ID'] == "5") { echo "checked"; } ?> value='5' id="gra5"><label for='gra5'>メンバー</label>
            </td>
          </tr>
        </table>
      </div>
      <div style="width:34%;float:right;margin:0px auto;">
        <input type="button" value="登録" onclick="conf();">
        <input type="reset" value="リセット">
        <input type="hidden" value="<?php echo $result[0]['member_ID'];?>" name="member_ID">
      </div>
    </form>
  </body>
</html>
