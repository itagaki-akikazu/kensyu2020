<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
<?php
 // common
 // include("./include/functions.php");
  $DB_DSN = "mysql:host=localhost; dbname=kennsyu_tesut; charset=utf8";
  $DB_USER = "itagaki";
  $DB_PW =  "pGVRdU7mQWnFxeGo";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
  // $odo = initDB();

  $search_name = "";
  $search_seibetu = "";
  $search_section_ID = "";
  $search_grade_ID = "";
  $search_grade_name = "";
  $search_section_name = "";

  if(isset($_GET['name']) && $_GET['name'] != ""){
    $search_name = $_GET['name'];
  }
  if(isset($_GET['seibetu']) && $_GET['seibetu'] != ""){
    $search_seibetu = $_GET['seibetu'];
  }
  if(isset($_GET['section_ID']) && $_GET['section_ID'] != ""){
    $search_section_ID = $_GET['section_ID'];
  }
  if(isset($_GET['grade_ID']) && $_GET['grade_ID'] != ""){
    $search_grade_ID = $_GET['grade_ID'];
  }
  if(isset($_GET['section_name']) && $_GET['section_name'] != ""){
    $search_section_name = $_GET['section_name'];
  }
  if(isset($_GET['grade_name']) && $_GET['grade_name'] != ""){
    $search_grade_name = $_GET['grade_name'];
  }

  $query_str = "SELECT member.member_ID, name,  seibetu, grade_master.grade_name, section1_master.section_name
                FROM member
                LEFT JOIN section1_master ON member.section_ID = section1_master.ID LEFT JOIN grade_master ON member.grade_ID = grade_master.ID
                WHERE 1 = 1";
  if(isset($_GET['name']) && $_GET['name'] != ""){
    $query_str .=" AND name LIKE '%". $_GET['name'] . "%'";
  }
  if(isset($_GET['seibetu']) && $_GET['seibetu'] != ""){
    $query_str .=" AND seibetu LIKE '%". $_GET['seibetu'] . "%'";
  }
  if(isset($_GET['section_ID']) && $_GET['section_ID'] != ""){
    $query_str .=" AND section_ID = '". $_GET['section_ID'] . "'";
  }
  if(isset($_GET['grade_ID']) && $_GET['grade_ID'] != ""){
    $query_str .=" AND grade_ID = '". $_GET['grade_ID'] . "'";
  }
  if(isset($_GET['grade_name']) && $_GET['grade_name'] != ""){
    $query_str .=" AND grade_name = '". $_GET['grade_name'] . "'";
  }
  if(isset($_GET['section_name']) && $_GET['section_name'] != ""){
    $query_str .=" AND section_name = '". $_GET['section_name'] . "'";
  }
  // $query_str = "SELECT * FROM 'test' WHERE menu LIKE '%の%' AND janru = 'おつまみ'"
  // echo $query_str;
  $sql = $pdo->prepare($query_str);
  $sql ->execute();
  $result = $sql->fetchAll();
  ?>
  <script type="text/javascript">
     <!--
     // スペースのサニタイズ
     function delSpace(){
       var temp = document.searchForm.namae.value;
       document.searchForm.namae.value = temp.replace(/\s+/g,"");
     }
     // フォームリセット
     function clearForm(){
       document.searchForm.name.value = "";
       document.searchForm.seibetu.value = "";
       document.searchForm.section_name.value = "";
       document.searchForm.grade_name.value = "";
     }
         -->
   </script>
<html>
  <head>
      <meta charset="utf-8">
      <meta name="Viewport" content="windth=device-width, initial-scale=l">
      <title>社員情報検索（#01）</title>
      <?php include("./include/header.php"); ?>
  </head>
  <body>
    <form method="GET" action="index.php" name="searchForm">
    <div style="width: 30%; margin:0 auto;">
      名前：<input type="text" name="name" value="<?php echo $search_name; ?>" placeholder="入力してください">
    </div>
    <div style="width: 30%; margin:0 auto;">
      性別：
    <select name="seibetu" size="1">
        <option value=""<?php if($search_seibetu == ""){echo "selected";}?>>すべて</option>
        <option value="1" <?php if($search_seibetu == "1"){echo "selected";}?>>男</option>
        <option value="2" <?php if($search_seibetu == "2"){echo "selected";}?>>女</option>
        <option value="3" <?php if($search_seibetu == "3"){echo "selected";}?>>その他</option>
    </select>
      部署：
    <select name="section_name" size="1">
        <option value=""<?php if($search_section_name == ""){echo "selected";}?>>すべて</option>
        <option value="第一事業部" <?php if($search_section_name == "第一事業部"){echo "selected";}?>>第一事業部</option>
        <option value="第二事業部" <?php if($search_section_name == "第二事業部"){echo "selected";}?>>第二事業部</option>
        <option value="営業" <?php if($search_section_name == "営業"){echo "selected";}?>>営業</option>
        <option value="総務" <?php if($search_section_name == "総務"){echo "selected";}?>>総務</option>
        <option value="人事部" <?php if($search_section_name == "人事部"){echo "selected";}?>>人事部</option>
    </select>
      役職：
    <select name="grade_name" size="1">
        <option value=""<?php if($search_grade_name == ""){echo "selected";}?>>すべて</option>
        <option value="事業部長" <?php if($search_grade_name == "事業部長"){echo "selected";}?>>事業部長</option>
        <option value="部長" <?php if($search_grade_name == "部長"){echo "selected";}?>>部長</option>
        <option value="チームリーダー" <?php if($search_grade_name == "チームリーダー"){echo "selected";}?>>チームリーダー</option>
        <option value="リーダー" <?php if($search_grade_name == "リーダー"){echo "selected";}?>>リーダー</option>
        <option value="メンバー" <?php if($search_grade_name == "メンバー"){echo "selected";}?>>メンバー</option>
    </select>
    </div>
    <div align="center">
      <input type="submit" value="検索" onClick="delSpace();">
      <input type="button" value="リセット" onClick='clearForm();'>
    </div>
    <div style="width: 30%; margin:0 auto;">
      検索結果：<?php echo count($result);?>
    </div>
    <div>
    <table border="1" align="center">
     <tr>
        <th>社員ID</th>
        <th>名前</th>
        <th>部署</th>
        <th>役職</th>
     </tr>
      <?php
      foreach($result as $each){
        echo "<tr>";
        echo  "<td>" . $each['member_ID']."</td>";
        echo "<td><a href='detail01.php?member_ID=$each[member_ID]'>".$each['name']."</a></td>";
        echo  "<td>" . $each['section_name']."</td>";
        echo  "<td>" . $each["grade_name"]."</td>";
        echo "</tr>";
      }
      ?>
    </table>
    </div>
  </body>
</html>
