
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>第二回課題、フォーム部品練習</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>第二回課題、フォーム部品練習</h1>
      <form method="post" action="syouhizei02.php">
        <table border="1">
            <th>商品名</th>
            <th>価格（単位：円、税抜き）</th>
            <th>個数</th>
            <th>税率</th>
          </tr>
          <tr>
            <td><input type="text" id="hinmei1" name="hinmei1"
              value="<?php echo $_POST['hinmei1'];?>">
            </td>
            <td><input type="text" id="kakaku1" name="kakaku1"
              value="<?php echo $_POST['kakaku1'];?>">
            </td>
            <td><input type="text" id="ko1" name="ko1"
              value="<?php echo $_POST['ko1'];?>">
              個
              </td>
            <td><input type="radio" id="zei1"name="zei1" value="1.08">8%
                <input type="radio" id="zei1"name="zei1" value="1.10">10%
            </td>
          </tr>
          <tr>
            <td><input type="text" id="hinmei2" name="hinmei2"
              value="<?php echo $_POST['hinmei2'];?>">
            </td>
            <td><input type="text" id="kakaku2" name="kakaku2"
              value="<?php echo $_POST['kakaku2'];?>">
            </td>
            <td>
              <input type="text" id="ko2" name="ko2"
              value="<?php echo $_POST['ko2'];?>">
            個
            </td>
            <td><input type="radio" id="zei2"name="zei2" value="1.08">8%
                <input type="radio" id="zei2"name="zei2" value="1.10">10%
            </td>
          </tr>
          <tr>
            <td><input type="text" id="hinmei3" name="hinmei3"
              value="<?php echo $_POST['hinmei3'];?>">
            </td>
            <td><input type="text" id="kakaku3" name="kakaku3"
              value="<?php echo $_POST['kakaku3'];?>">
            </td>
            <td><input type="text" id="ko3" name="ko3"
              value="<?php echo $_POST['ko3'];?>">
              個
            </td>
            <td><input type="radio" id="zei3"name="zei3" value="1.08">8%
                <input type="radio" id="zei3"name="zei3" value="1.10">10%
          </tr>
          <tr>
            <td><input type="text" id="hinmei4" name="hinmei4"
             value="<?php echo $_POST['hinmei4'];?>">
          </td>
            <td><input type="text" id="kakaku4" name="kakaku4"
             value="<?php echo $_POST['kakaku4'];?>">
            </td>
            <td><input type="text" id="ko4" name="ko4"
             value="<?php echo $_POST['ko4'];?>">
            個
            </td>
            <td><input type="radio" id="zei4"name="zei4" value="1.08">8%
                <input type="radio" id="zei4"name="zei4" value="1.10">10%
            </td>
          </tr>
            <tr>
              <td><input type="text" id="hinmei5" name="hinmei5"
             value="<?php echo $_POST['hinmei5'];?>">
            </td>
              <td><input type="text" id="kakaku5" name="kakaku5"
             value="<?php echo $_POST['kakaku5'];?>">
              </td>
              <td><input type="text" id="ko5" name="ko5"
             value="<?php echo $_POST['ko5'];?>">
            個
          </td>
              <td><input type="radio" id="zei5"name="zei5" value="1.08">8%
                  <input type="radio" id="zei5"name="zei5" value="1.10">10%
              </td>
            </tr>
        </table>
      <div>
            <td colspan="1"<td align="left"><input type="submit" value="送信"><input type="reset" value="取り消し"></td>
      </div>
      <table border="1">
        <tr>
          <th>商品名</th>
          <th>価格（単位：円、税抜き）</th>
          <th>個数</th>
          <th>税率</th>
          <th>小計（単位：円）</th>
        </tr>
        <tr>
          <td>
            <?php
              echo $_POST['hinmei1'];
            ?>
          </td>
          <td>
            <?php
              echo number_format($_POST['kakaku1']);
            ?>
          </td>
          <td>
            <?php
              echo number_format($_POST['ko1']);
            ?>
          個
        </td>
          <td>
            <?php
              echo $_POST['zei1'];
            ?>
          </td>
          <td>
            <?php
            $price1 = $_POST['kakaku1'];
            $price2 = $_POST['ko1'];
            $price3 = $price1 * $price2;
            $price4 = $_POST['zei1'];
            $price31 = $price3 * $price4;
            round($price31 . "円（税込み");
            echo number_format($price31)?>円</td>
          </td>
        </tr>
        <tr>
          <td>
            <?php
            echo $_POST['hinmei2'];
            ?>
          </td>
          <td>
            <?php
            echo number_format($_POST['kakaku2']);
            ?>
          </td>
          <td>
            <?php
            echo number_format($_POST['ko2']);
            ?>
            個
          </td>
          <td>
            <?php
              echo $_POST['zei2'];
            ?>
          </td>
          <td>
            <?php
            $price1 = $_POST['kakaku2'];
            $price2 = $_POST['ko2'];
            $price3 = $price1 * $price2;
            $price4 = $_POST['zei2'];
            $price32 = $price3 * $price4;
            round($price32 . "円（税込み");
            echo number_format($price32)?>円</td>
          </td>
        </tr>
        <tr>
          <td>
            <?php
            echo $_POST['hinmei3'];
            ?>
          </td>
          <td>
            <?php
            echo number_format($_POST['kakaku3']);
            ?>
          </td>
          <td>
            <?php
            echo number_format($_POST['ko3']);
            ?>
            個
          </td>
          <td>
            <?php
            echo $_POST['zei3'];
            ?>
          </td>
          <td>
            <?php
            $price1 = $_POST['kakaku3'];
            $price2 = $_POST['ko3'];
            $price3 = $price1 * $price2;
            $price4 = $_POST['zei3'];
            $price33 = $price3 * $price4;
            round($price33 . "円（税込み");
            echo number_format($price33)?>円</td>
          </td>
        </tr>
        <tr>
          <td>
            <?php
            echo $_POST['hinmei4'];
            ?>
          </td>
          <td>
            <?php
            echo number_format($_POST['kakaku4']);
            ?>
          </td>
          <td>
            <?php
            echo number_format($_POST['ko4']);
            ?>
            個
          </td>
          <td>
            <?php
            echo $_POST['zei4'];
            ?>
          </td>
          <td>
            <?php
            $price1 = $_POST['kakaku4'];
            $price2 = $_POST['ko4'];
            $price3 = $price1 * $price2;
            $price4 = $_POST['zei4'];
            $price34 = $price3 * $price4;
            round($price34 . "円（税込み");
            echo number_format($price34)?>円</td>
          </td>
          <tr>
            <td>
              <?php
              echo $_POST['hinmei5'];
              ?>
            </td>
            <td>
              <?php
              echo number_format($_POST['kakaku5']);
              ?>
            </td>
            <td>
              <?php
              echo number_format($_POST['ko5']);
              ?>
              個
            </td>
            <td>
              <?php
              echo $_POST['zei5'];
              ?>
            </td>
            <td>
              <?php
              $price1 = $_POST['kakaku5'];
              $price2 = $_POST['ko5'];
              $price3 = $price1 * $price2;
              $price4 = $_POST['zei5'];
              $price35 = $price3 * $price4;
              round($price35 . "円（税込み");
              echo number_format($price35)?>円</td>
            </td>
          </tr>
            <td>
            合計
            </td>
            <td>
              <?php
              $sub01 = $price31;
              $sub02 = $price32;
              $sub03 = $price33;
              $sub04 = $price34;
              $sub05 = $price35;
              $total = $sub01 + $sub02 + $sub03 + $sub04 + $sub05;
              round($total . "円（税込み");
              echo number_format($total);
              ?>
            </td>
          </tr>
      </table>

      </form>

  </body>
</html>
